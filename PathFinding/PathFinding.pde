NodeMap worldMap;

int deltaTime = 0;
int previousTime = 0;

int mapRows = 100;
int mapCols = 100;

color baseColor = color (0, 127, 0);

boolean waypoint=false;

void setup () {
  //size (420, 420);
  fullScreen();
  
  initMap();
}

void draw () {
  deltaTime = millis () - previousTime;
  previousTime = millis();
  
  update(deltaTime);
  display();
}

void update (float delta) {
  
}

void display () {
  worldMap.display();
}

void initMap () {
  worldMap = new NodeMap (mapRows, mapCols); 
  
  worldMap.setBaseColor(baseColor);
  
  worldMap.makeWall (mapCols / 2, 0, 15, true);
    worldMap.makeWall (mapCols / 2 - 9, 10, 10, false);
  
  if(!waypoint)
  {
    
    
    worldMap.setStartCell(int(random(1, 99)), int(random(1, 99)));
    do
    {
      worldMap.setEndCell(int(random(1, 99)), int(random(1, 99)));
    }while(!worldMap.end.isWalkable);
    
    worldMap.updateHs();
      
    worldMap.generateNeighbourhood();
        
    worldMap.findAStarPath();
  }
  else
  {
    worldMap.setStartCell(17, 44);
    worldMap.setEndCell(30, 66);
    
    worldMap.updateHs();
      
    worldMap.generateNeighbourhood();
        
    worldMap.findAStarPath();
    
    worldMap.setStartCell(30, 66);
    worldMap.setEndCell(66, 30);
    
    worldMap.updateHs();
      
    worldMap.generateNeighbourhood();
        
    worldMap.findAStarPath();
    
    worldMap.setStartCell(66, 30);
    worldMap.setEndCell(44, 17);
    
    worldMap.updateHs();
      
    worldMap.generateNeighbourhood();
        
    worldMap.findAStarPath();
  }
}

void keyPressed()
{
    if(key == 'r')
      setup();
      
    if(key == 'w')
    {
      if(waypoint)
      waypoint = false;
      else
      waypoint  =true;
      
      setup();
  }
}
