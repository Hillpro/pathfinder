/********************
  Énumération des directions
  possibles
  
********************/
enum Direction {
  EAST, SOUTH, WEST, NORTH
}

/********************
  Représente une carte de cellule permettant
  de trouver le chemin le plus court entre deux cellules
  
********************/
class NodeMap extends Matrix {
  Node start;
  Node end;
  
  ArrayList <Node> totalPath = new ArrayList<Node>();
  ArrayList <Node> path;
  
  boolean debug = false;
  
  NodeMap (int nbRows, int nbColumns) {
    super (nbRows, nbColumns);
  }
  
  NodeMap (int nbRows, int nbColumns, int bpp, int width, int height) {
    super (nbRows, nbColumns, bpp, width, height);
  }
  
  void init() {
    
    cells = new ArrayList<ArrayList<Cell>>();
    
    for (int j = 0; j < rows; j++){
      // Instanciation des rangees
      cells.add (new ArrayList<Cell>());
      
      for (int i = 0; i < cols; i++) {
        Cell temp = new Node(i * cellWidth, j * cellHeight, cellWidth, cellHeight);
        
        // Position matricielle
        temp.i = i;
        temp.j = j;
        
        cells.get(j).add (temp);
      }
    }
    
    println ("rows : " + rows + " -- cols : " + cols);
  }
  
  /*
    Configure la cellule de départ
  */
  void setStartCell (int i, int j) {
    
    if (start != null) {
      start.isStart = false;
      start.setFillColor(baseColor);
    } 
    
    start = (Node)cells.get(j).get(i);
    start.isStart = true;
    
    start.setFillColor(color (0, 0, 255));
  }
  
  /*
    Configure la cellule de fin
  */
  void setEndCell (int i, int j) {
    
    if (end != null) {
      end.isEnd = false;
    }
    
    end = (Node)cells.get(j).get(i);
    end.isWalkable = true;
    end.isEnd = true;
  }
  
  /** Met a jour les H des cellules
  doit etre appele apres le changement du noeud
  de debut ou fin
  */
  void updateHs() {
    for (int j = 0; j < rows; j++) {
      for (int i = 0; i < cols; i++) {
        Node current = (Node)cells.get(j).get(i); 
        current.setH( calculateH(current));
      }
    }
  }
  
  // Permet de generer aleatoirement le cout de deplacement
  // entre chaque cellule
  void randomizeMovementCost() {
    for (int j = 0; j < rows; j++) {
      for (int i = 0; i < cols; i++) {
        
        int cost = parseInt(random (0, cols)) + 1;
        
        Node current = (Node)cells.get(j).get(i);
        current.setMovementCost(cost);
       
      }
    }
  }
  
  // Permet de generer les voisins de la cellule a la position indiquee
  void generateNeighbours(int i, int j) {
    Node c = (Node)getCell (i, j);
    if (debug) println ("Current cell : " + i + ", " + j);
    
    
    for (Direction d : Direction.values()) {
      Node neighbour = null;
      
      switch (d) {
        case EAST :
          if (i < cols - 1) {
            if (debug) println ("\tGetting " + d + " neighbour for " + i + ", " + j);
            neighbour = (Node)getCell(i + 1, j);
          }
          break;
        case SOUTH :
          if (j < rows - 1) {
            if (debug) println ("\tGetting " + d + " neighbour for " + i + ", " + j);
            neighbour = (Node)getCell(i, j + 1);
          }
          break;
        case WEST :
          if (i > 0) {
            if (debug) println ("\tGetting " + d + " neighbour for " + i + ", " + j);
            neighbour = (Node)getCell(i - 1, j);
          }
          break;
        case NORTH :
          if (j > 0) {
            if (debug) println ("\tGetting " + d + " neighbour for " + i + ", " + j);
            neighbour = (Node)getCell(i, j - 1);
          }
          break;
      }
      
      if (neighbour != null) {
        if (neighbour.isWalkable) {
          c.addNeighbour(neighbour);
        }
      }
    }
  }
  
  /**
    Génère les voisins de chaque Noeud
  */
  void generateNeighbourhood() {
    for (int j = 0; j < rows; j++) {
      for (int i = 0; i < cols; i++) {

        generateNeighbours(i, j);
      }
    }
  }
  
  /*
    Permet de trouver le chemin le plus court entre
    deux cellules
  */
  void findAStarPath () {
    
    if (start == null || end == null) {
      println ("No start and no end defined!");
      return;
    }
    
    ArrayList<Node> openList = new ArrayList<Node>();
    ArrayList<Node> closedList = new ArrayList<Node>();
    openList.add(start);
    
    
    Node n = null;
    int i=0;
    
    while(openList.size()!=0)
    {
      n = getLowestCost(openList);
      
      if(n != end)
      {
        openList.remove(n);
        
        if(n != end && n != start)
        //n.setFillColor(color(0, 255, 100));
        closedList.add(n);
        
        for(Node adj : n.neighbours)
        {
          
          if(adj.isWalkable == true && !closedList.contains(adj))
          {
            int gPrime = calculateCost(n, adj) + n.getG();
            
            if (gPrime >= adj.getG())
            {
              adj.setParent(n);
              
              if(!openList.contains(adj))
              {
                //adj.setFillColor(color(255, 135, 0));
                openList.add(adj);
              }
            }
          }
        }
      }
      else
      {
        generatePath();
        
        break;
      }
    }
    
    println("finished");
  }
  
  /*
    Permet de générer le chemin une fois trouvée
  */
  void generatePath () {
    
    path = new ArrayList<Node>();
    Node p = end;
    
    end.setFillColor(color(255, 255, 0));
    while(p != start)
    {
      if(p != end)
      p.setFillColor(color (127, 0, 0));
      
      path.add(p);
      p = p.parent;
      totalPath.add(p);
    }
    
    end = null;
    start = null;
  }
  
  /**
  * Cherche et retourne le noeud le moins couteux de la liste ouverte
  * @return Node le moins couteux de la liste ouverte
  */
  private Node getLowestCost(ArrayList<Node> openList) {
    
    Node lowest = openList.get(0);
    
    for (Node n : openList)
    {
      if(n.getF() < lowest.getF())
      {
        lowest = n;
      }
    }
    
    return lowest;
  }
  

  
 /**
  * Calcule le coût de déplacement entre deux noeuds
  * @param nodeA Premier noeud
  * @param nodeB Second noeud
  * @return
  */
  private int calculateCost(Cell nodeA, Cell nodeB) {
    
    int distanceX = 0;
    int distanceY = 0;
    
    distanceX = nodeA.i - nodeB.i;
    
    if(distanceX < 0) { distanceX *= -1;}
    
    distanceY = nodeA.j - nodeB.j;
    
    if(distanceY < 0) { distanceY *= -1;}
    
    if (distanceX + distanceY == 2)
    {
      return 14;
    }
    else
    {
      return (distanceX + distanceY * 10);
    }
  }
  
  int refreshAcc = 0;
  int refreshRate=50;
  int compteur = 0;
  
  @Override
  void display()
  {
    refreshAcc+=deltaTime;
    if(refreshAcc >= refreshRate)
    {
      refreshAcc = 0; //<>//
      if(totalPath != null)
      {
        Node n = totalPath.get(compteur++);
        n.setFillColor(color(255));
        if(compteur >= totalPath.size())
        {
          compteur = 0;
        }
      }
    }
    super.display();
  }
  
  /**
  * Calcule l'heuristique entre le noeud donnée et le noeud finale
  * @param node Noeud que l'on calcule le H
  * @return la valeur H
  */
  private int calculateH(Cell node) {
    
    return calculateCost(end, node);
  }
  
  String toStringFGH() {
    String result = "";
    
    for (int j = 0; j < rows; j++) {
      for (int i = 0; i < cols; i++) {

        Node current = (Node)cells.get(j).get(i);
        
        result += "(" + current.getF() + ", " + current.getG() + ", " + current.getH() + ") ";
      }
      
      result += "\n";
    }
    
    return result;
  }
  
  // Permet de créer un mur à la position _i, _j avec une longueur
  // et orientation données.
  void makeWall (int _i, int _j, int _length, boolean _vertical) {
    int max;
    
    if (_vertical) {
      max = _j + _length > rows ? rows: _j + _length;  
      
      for (int j = _j; j < max; j++) {
        ((Node)cells.get(j).get(_i)).setWalkable (false, 0);
      }       
    } else {
      max = _i + _length > cols ? cols: _i + _length;  
      
      for (int i = _i; i < max; i++) {
        ((Node)cells.get(_j).get(i)).setWalkable (false, 0);
      }     
    }
  }
}
